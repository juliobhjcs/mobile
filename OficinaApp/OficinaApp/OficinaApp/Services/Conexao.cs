﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace OficinaApp.Services
{
  public  class Conexao
    {
        public T Post<T>(T parametro, string caminho)
        {
            try
            {
                HttpClient client = new HttpClient();
                // Requisição HTTP
                Uri url = new Uri("http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/Api/Indicacao");
                //url base da webapi
                //criando uma instancia objeto de retorno
                var retorno = Activator.CreateInstance<T>();

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //json que será enviado a webapi                  
                var paramJson = Newtonsoft.Json.JsonConvert.SerializeObject(parametro);
                HttpContent content = new StringContent(paramJson, Encoding.UTF8, "application/json");
                //retorno do Post realizado na webApi
                HttpResponseMessage response = client.PostAsync(url, content).Result;

                //se o retorno da webapi foi sucesso, converte o json retornado para o objeto(class) em questao (T)
                if (response.IsSuccessStatusCode)
                {
                    retorno = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);

                }

                return retorno;

            }
            catch (Exception ex)
            {

            }
            return default(T);
        }
        public T GET<T>(T paramentro, string codido)
        {

            try
            {
                // Cria uma instancia de objetos
                var retorno = Activator.CreateInstance<T>();
                // Requisição HTTP
                using (var cliente = new HttpClient())
                {
                    //url base da webapi
                    Uri uri = new Uri("http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/Api/Oficina?codigoAssociacao=");

                    //chamada da url da webapi
                    HttpResponseMessage response = cliente.GetAsync(uri + codido).Result;

                    // Se o retorno obteve sucesso
                    if (response.IsSuccessStatusCode)
                    {
                        //response.Content.ReadAsStringAsync().Result =>Retorno da API
                        // JsonConvert.DeserializeObject<T>(json) => Converte objeto em json
                        retorno = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    }
                    return retorno;
                }
            }
            catch (Exception ex)
            {

            }
            return default(T);
        }
    }
}

﻿using Android.Content.Res;
using Android.Graphics;
using Android.Util;
using System;

using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OficinaApp
{
   public static class Util
    {
        public static Bitmap ConverterBase64EmBitmap(string stringBase64)
        {

            if (!string.IsNullOrWhiteSpace(stringBase64))
            {
                byte[] decodedBytes = Base64.Decode(stringBase64, Base64Flags.Default);
                return BitmapFactory.DecodeByteArray(decodedBytes, 0, decodedBytes.Length);
            }

            return null;
        }

      
    }
}

﻿using Android.Content;
using Android.Graphics;
using Android.Widget;
using OficinaApp.Models;
using OficinaApp.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OficinaApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetalhesOficina : ContentView
    {
        Conexao conectar = new Conexao();
        ClasseIndicacao classeIndicacao = new ClasseIndicacao();
        ClasseEntradaIndicacao indicacao = new ClasseEntradaIndicacao();
        public object selectedItem;
        

        public DetalhesOficina(object selectedItem)
        {
            InitializeComponent();

            this.selectedItem = selectedItem;

            txtfoto.BindingContext = selectedItem; // recebe o valor string ref a foto
            string imagem = txtfoto.Text;
            
            Bitmap Base64Stream = Util.ConverterBase64EmBitmap(imagem);
            imgfoto.BindingContext = Base64Stream;



            imgfoto.BindingContext = selectedItem;
            txtnome.BindingContext = selectedItem;
            txtendereco.BindingContext = selectedItem;
            txttelefone1.BindingContext = selectedItem;
            txtdescricaocurta.BindingContext = selectedItem;
            codigo.BindingContext = selectedItem;
            email.BindingContext = selectedItem;

        }

        private void btnindicar_Clicked(object sender, EventArgs e)
        {
            txttelindicado.IsVisible = true;
            emailindicado.IsVisible = true;
            email.IsVisible = true;
            codigo.IsVisible = true;
            txtnomeindicado.IsVisible = true;
            btnenviar.IsVisible = true;
            
        }

        private void btnenviar_Clicked(object sender, EventArgs e)
        {
            try
            {
                var conn = conectar.Post<ClasseEntradaIndicacao>(indicacao, email.Text);

                if(conn!= null)
                {
                    Content = new Indicacao();

                }
                else
                {
                    
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
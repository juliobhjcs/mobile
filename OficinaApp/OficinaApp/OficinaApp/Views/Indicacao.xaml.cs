﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OficinaApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Indicacao : ContentView
	{
		public Indicacao ()
		{
			InitializeComponent ();
		}

        private void btnvoltar_Clicked(object sender, EventArgs e)
        {
            Content = new OficinaPage();
        }
    }
}
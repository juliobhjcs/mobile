﻿using Android.Content;
using Android.Widget;
using OficinaApp.Models;
using OficinaApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OficinaApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OficinaPage : ContentView
	{
        Conexao conectar = new Conexao();
        ClasseRetornoOficina retornoOficina = new ClasseRetornoOficina();
        List<ClasseOficina> Lista = new List<ClasseOficina>();
        Context context;

        public OficinaPage ()
		{
			InitializeComponent ();
            BindingContext = new List<ClasseOficina>();
        }

        private void btnbuscar_Clicked(object sender, EventArgs e)
        {
            if(txtcodigo.Text == "")
            {
                Toast.MakeText(context, "Campo de codigo não pode ser vázio", ToastLength.Long).Show();
            }
            else
            {
            // faz a conexao get com a webapi
            var conn = conectar.GET<ClasseRetornoOficina>(retornoOficina, txtcodigo.Text);

            // se o retorno obteve sucesso
            if(conn != null)
            {
                // lista de oficinas recebe objetos de retorno
                Lista = conn.listaOficinas;
                // listview recebe parametros
                listaoficina.ItemsSource = Lista;
                
                
            }
            }
        }

        private void listaoficina_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

        Content =  new  DetalhesOficina(listaoficina.SelectedItem).Content;

          


        }
    }
}
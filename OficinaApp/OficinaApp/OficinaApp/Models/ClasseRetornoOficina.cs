﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OficinaApp.Models
{
   public class ClasseRetornoOficina
    {
        public List<ClasseOficina> listaOficinas { get; set; }
        public RetornoErro RetornoErros { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace OficinaApp.Models
{
   public class ClasseOficina
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string DescricaoCurta { get; set; }
        public string Endereco { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Foto { get; set; }
        public int AvaliacaoUsuario { get; set; }
        public string CodigoAssociacao { get; set; }
        public string Email { get; set; }
        public string Telefone1 { get; set; }
        public string Telefone2 { get; set; }
        public bool Ativo { get; set; }
        public Page page { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OficinaApp.Models
{
   public class ClasseEntradaIndicacao
    {
        public string Remetentes { get; set; }
        public string Copias { get; set; }
        public ClasseIndicacao ClasseIndicacao { get; set; }
    }
}
